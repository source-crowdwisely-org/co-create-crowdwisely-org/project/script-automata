#!/bin/bash

# Ensure we're in a Git repository
if ! git rev-parse --git-dir > /dev/null 2>&1; then
    echo "Error: must be run from within a Git repository"
    exit 1
fi

# Update main branch from its upstream
git checkout main && git pull 'source' main

# Checkout the github-main branch
git checkout github-main

# Merge changes from main into github-main, handling conflicts if they occur
git merge main || {
  echo "Merge conflict detected. Please resolve conflicts then run the script again."
  exit 1
}

# Checkout the main branch again
git checkout main


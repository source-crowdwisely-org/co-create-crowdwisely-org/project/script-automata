#!/usr/bin/env bash
# -------------------------------------------------------------------------
# Convert all characters from file input to lowercase
# Write a shell script that changes the name of the file passed as an argument
# to lowercase.
# -------------------------------------------------------------------------
# Copyright (c) 2003 nixCraft project <http://cyberciti.biz/fb/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of nixCraft shell script collection (NSSC)
# Visit http://bash.cyberciti.biz/ for more information.
# -------------------------------------------------------------------------
# Reference Sources:
# * https://bash.cyberciti.biz/file-management/shell-script-rename-file-names-lowercase/
# -------------------------------------------------------------------------

convert_filename_to_lowercase() {
    # Check if an argument is passed
    if [ $# -eq 0 ]; then
        echo "Usage: $(basename $0) <file-name>"
        exit 1
    fi

    local file="$1"

    # Check if the file exists
    if [ ! -f "$file" ]; then
        echo "Error: '$file' not a file"
        exit 2
    fi

    # Convert filename to lowercase
    local lowercase=$(echo $file | tr '[A-Z]' '[a-z]')

    # Check if the converted filename already exists
    if [ -f "$lowercase" ]; then
        echo "Error: File '$lowercase' already exists!"
        exit 3
    fi

    # Rename the file
    /bin/mv "$file" "$lowercase"
}

# Call the function with the passed argument
convert_filename_to_lowercase "$1"

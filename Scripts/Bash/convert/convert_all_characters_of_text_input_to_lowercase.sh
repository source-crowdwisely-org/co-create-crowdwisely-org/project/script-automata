#!/usr/bin/env bash

# Define the function to convert all characters of text input to lowercase
convert_all_characters_of_text_input_to_lowercase() {
    declare -l txt="$1"
    echo "$txt"
}

# Example usage of the function
# This part can be modified or removed depending on how you want to use the function

# Read input from the command line argument
input_text="$1"

# Check if input was provided
if [ -z "$input_text" ]; then
    echo "Usage: $0 \"TEXT\""
    exit 1
fi

# Call the function with the provided input
convert_all_characters_of_text_input_to_lowercase "$input_text"

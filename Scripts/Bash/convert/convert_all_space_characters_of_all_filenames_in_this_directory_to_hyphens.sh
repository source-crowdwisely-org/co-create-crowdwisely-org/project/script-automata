#!/usr/bin/env bash

# Function to convert all spaces in filenames within the current directory to hyphens
convert_all_space_characters_of_all_filenames_in_this_directory_to_hyphens() {
    # Loop through all files in the current directory
    for file in *; do
        # Use sed to replace spaces with hyphens in the filename
        local new_name=$(echo "$file" | sed 's/ /-/g')
        # Check if the new filename differs from the original
        if [ "$file" != "$new_name" ]; then
            # Move (rename) the file to the new filename
            mv -n "$file" "$new_name"
        fi
    done
}

# Call the function
convert_all_space_characters_of_all_filenames_in_this_directory_to_hyphens

#!/usr/bin/env bash

# Function to convert all characters of all filenames in the current directory to lowercase
convert_all_filenames_to_lowercase() {
    for file in *; do
        # Convert the filename to lowercase using tr
        local new_name=$(echo "$file" | tr '[:upper:]' '[:lower:]')
        # Check if the new filename differs from the original
        if [ "$file" != "$new_name" ]; then
            # Move (rename) the file to the new filename, avoiding overwrites
            mv -n "$file" "$new_name"
        fi
    done
}

# Call the function
convert_all_filenames_to_lowercase

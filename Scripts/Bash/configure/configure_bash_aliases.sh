## become root ##
alias root="sudo -i"
alias su="sudo -i"

## Updating, upgrading, and cleaning the Ubuntu System  ##
alias ubsupstatus="ubuntu-support-status"
alias upgd="upgrayedd"
alias upgdd="upgrayeddd"

## Update Anbox
alias update-anbox="snap refresh --beta --devmode anbox"

## Restart sound
alias restart-sound="pulseaudio -k"

# Make some of the file manipulation programs verbose
alias mv="mv -v"
alias rm="rm -vi --preserve-root"
alias cp="cp -v"

# Parenting changing perms on / #
alias chown="chown --preserve-root"
alias chmod="chmod --preserve-root"
alias chgrp="chgrp --preserve-root"

# User management
alias list-users="cut -d: -f1 /etc/passwd"
alias list-super-users="getent group sudo | cut -d: -f4" #inspiredBy: https://askubuntu.com/questions/611584/how-could-i-list-all-super-users#611607

## Colorize the ls output ##
alias ls="exa -h"
 
## Use a long listing format ##
alias ll="ls -l"
alias lla="ls -la"
 
## Show hidden files ##
alias l.="ls -d .* --color=auto"

## Show which shell I am currently using
alias which-shell='echo $0'

## Checking versions of tools
alias java-version="java -version"
alias javac-version="javac -version"
alias maven-version="mvn -v"
alias gradle-version="gradle -v"

## Command shortcuts to save time ""
alias h="history"
alias j="jobs -l"

## List all PPAs ##
alias listppa="apt-cache policy | grep http | awk '{print $2 $3}'"

## List all User Installed Packages ##
alias uinstpack="comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u)"

## Find out which PPA a package belongs to ##
alias acp="apt-cache policy"

## get rid of command not found ##
#alias cd..="cd .."
 
## a quick way to get out of current directory ##
alias ..="cd .."
alias .2="cd ../../"
alias ...="cd ../../../"
alias ....="cd ../../../../"
alias .....="cd ../../../../"
alias .4="cd ../../../../"
alias .5="cd ../../../../.."

## a quick way to go back to previous directory ##
alias -- -="cd -"

## Create parent directories on demand ##
alias mkdir="mkdir -pv"

## Open a file ##
alias openn="xdg-open"

## Opens current directory in a file explorer ##
alias explore="nautilus ."

## Opens current directory in a file explorer with super user privileges ##
alias suexplore="sudo caja ."

## Opens a GUI text editor in the background. Can obviously be replaced with your favorite editor ##
alias text="pluma &"
alias sutext="gksudo pluma &" #Same as above with super user privileges

#Opens a file with whatever program would open by double clicking on it in a GUI file explorer
#Usage: try someDocument.doc
alias try='xdg-open'

#show aliases
alias a='echo "------------Your aliases------------";alias'
alias show-alias-value='alias | grep $1'

# Prints disk usage in human readable form
alias disk-usage="df -h"
alias disk-usage-sorted="du -sh -- * | sort -h"
alias disk-usage-sorted-hidden="du -sh .[!.]* * | sort -h"
alias folder-usage="du -ch"
alias total-folder-usage="du -sh"

## Make mount command output pretty and human readable format ##
alias mount="mount |column -t"

## Lists all available HDDs/partitions ##
alias list-all-mounted-drives="sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL"

# Clear the screen of your clutter
alias c="clear"
alias cl="clear;ls;pwd"

## Colorize diff output ##
alias diff="colordiff"

## Colorize the grep command output for ease of use (good for log files)##
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

# Edit shortcuts for config files
alias sshconfig="edit ~/.ssh/config"
alias bashrc="edit ~/.bashrc && source ~/.bashrc && echo Bash config edited and reloaded."
alias bash_aliases="edit ~/source/co-create/project/Script-Automata/scripts/bash/bash_aliases && source ~/.bashrc && echo Bash aliases edited and reloaded."
alias bash_functions="edit ~/source/co-create/project/Script-Automata/scripts/bash/bash_functions && source ~/.bashrc && echo Bash functions edited and reloaded."
alias zshrc="edit ~/.zshrc && source ~/.zshrc && echo Zsh config edited and reloaded."
alias zsh_aliases="edit ~/.zsh_aliases && source ~/.zshrc && echo Zsh aliases edited and reloaded."

## Start calculator with math support ##
alias bc="bc -l"

## View running processes ##
alias top="htop" 

# SSH helper
alias sshclear="rm ~/.ssh/multiplex/* -f && echo SSH connection cache cleared;"
alias sshlist="echo Currently open ssh connections && echo && l ~/.ssh/multiplex/"

#4: Generate sha256 digest
alias sha256="openssl sha256"

## Create a new set of commands ##
alias path="echo -e ${PATH//:/\\n}"
alias nowtime="date +'%T'"
alias nowdate="date +'%d-%m-%Y'"
alias now="nowdate;nowtime"

## Set vim as default ##
alias vi=vim
#alias svi="sudo vi"
#alias vis="vim '+set si'"
alias edit="vim"

# Remove all SSL Certificates
alias remove-all-ssl-certs="sudo rm -rf /etc/letsencrypt/{live,renewal,archive}/{${DOMAIN},${DOMAIN}.conf}"

## Control output of networking tool called ping ##
alias ping="ping -c 5" # Stop after sending count ECHO_REQUEST packets
alias fastping="ping -c 100 -s.2" # Do not wait interval 1 second, go fast

# get web server headers #
alias header='curl -I'

## Show open ports ##
alias ports="sudo netstat -tulanp"

# reboot / halt / poweroff
alias reboot="sudo /sbin/reboot"
alias graceful-reboot="graceful-docker-shutdown && reboot"
alias poweroff="sudo /sbin/poweroff"
alias halt="sudo /sbin/halt"
alias shutdown="sudo /sbin/shutdown"

# remove old kernels #
alias remove-old-kernels="sudo sh /opt/purge-old-kernels-2.sh; sudo rm -v /boot/*.old-dkms"

# Update all Pip packages
alias pipupd8="pip3 freeze | grep -v '^-e' | cut -d = -f 1  | xargs -n1 pip3 install --user -U"

# Create posters with Scribus ##
alias poster="scribus-ng"

# Connect to vpn th-koeln #
alias vpn-th-koeln="sudo openconnect https://vpn-gm.th-koeln.de -u jseidler -i vpn0"

# Connect to Rapsberry Pi #
alias connect-to-pi="ssh pi@192.168.0.31"

# Convert html to pdf
alias htmltopdf="wkhtmltopdf"

# Unzip all files in current directory
alias unzipall="unzip \*.zip"

# Hardware details
alias full-specs="sudo lshw | less"
alias specs="lspci"
alias verbose-specs="lspci -v"
alias very-verbose-specs="lspci -vv"
alias specs-graphics="lspci -nnk | grep VGA -A1"
alias specs-audio="lspci -v | grep -A7 -i 'audio'"
alias specs-networking="lspci -nnk | grep net -A2"
alias display-full-system-information="inxi -F"

# Version Control
alias gs="git status"
alias gd="git add ."
#alias gc="git commit -a -m '$i'"
#alias gp="git push -u origin master"
alias gdf="git diff"
alias glock="git diff --ignore-all-space --ignore-space-at-eol --ignore-space-change --ignore-blank-lines -- . ':(exclude)*package-lock.json'"
alias git-multi-push="git push source main && git push public-mirror main && git push private-mirror main"
alias gmp="git-multi-push"
alias gpar="git_push_all_remotes"

## Running Apps
alias anki="flatpak run net.ankiweb.Anki"

# Docker
# more at https://github.com/htpcBeginner/docker-traefik/blob/master/.bash_aliases.example
alias dstopcont='sudo docker stop $(docker ps -a -q)'
alias dstopall='sudo docker stop $(sudo docker ps -aq)'
alias drmcont='sudo docker rm $(docker ps -a -q)'
alias dvolprune='sudo docker volume prune'
alias dsysprune='sudo docker system prune -a'
alias ddelimages='sudo docker rmi $(docker images -q)'
alias docerase='dstopcont ; drmcont ; ddelimages ; dvolprune ; dsysprune'
alias docprune='ddelimages ; dvolprune ; dsysprune'
alias dexec='sudo docker exec -ti'
alias docps='sudo docker ps -a'
alias dcrm='dcrun rm'
alias docdf='sudo docker system df'
alias dclogs='sudo docker logs -tf --tail="50" '
alias fixsecrets='sudo chown -R root:root /home/USER/docker/secrets ; sudo chmod -R 600 /home/USER/docker/secrets'

# DOCKER TRAEFIK 2
alias dcrun2='cd /home/USER/docker ; sudo docker-compose -f /home/USER/docker/docker-compose-t2.yml '
alias dclogs2='cd /home/USER/docker ; sudo docker-compose -f /home/USER/docker/docker-compose-t2.yml logs -tf --tail="50" '
alias dcup2='dcrun2 up -d'
alias dcdown2='dcrun2 down'
alias dcrec2='dcrun2 up -d --force-recreate'
alias dcstop2='dcrun2 stop'
alias dcrestart2='dcrun2 restart '
alias dcpull2='cd /home/USER/docker ; sudo docker-compose -f /home/USER/docker/docker-compose-t2.yml  pull'

# DOCKER TRAEFIK 2 VPN
alias dcrun2v='cd /home/USER/docker ; sudo docker-compose -f /home/USER/docker/docker-compose-t2-vpn.yml '
alias dclogs2v='cd /home/USER/docker ; sudo docker-compose -f /home/USER/docker/docker-compose-t2-vpn.yml logs -tf --tail="50" '
alias dcup2v='dcrun2v up -d'
alias dcdown2v='dcrun2v down'
alias dcrec2v='dcrun2v up -d --force-recreate'
alias dcstop2v='dcrun2v stop'
alias dcrestart2v='dcrun2v restart '
alias dcpull2v='cd /home/USER/docker ; sudo docker-compose -f /home/USER/docker/docker-compose-t2-vpn.yml  pull'

# NETWORKING
alias portsused='sudo netstat -tulpn | grep LISTEN'

# FILE SIZE AND STORAGE
alias free='free -h'
alias fdisk='sudo fdisk -l'
alias uuid='sudo vol_id -u'
alias ll='ls -alh'
alias dirsize='sudo du -hx --max-depth=1'

# Copy text to clipboard
alias copy-text-to-clipboard='xclip -selection c'

# Useful Python 3 aliases
alias py='python3'
alias python='python3'
alias pip='pip3'
alias pyvenv='virtualenv'
alias pyactivate='source env/bin/activate'
alias pydeactivate='deactivate'

# Connect via ssh to crowdwisely.org server
alias crwsly.org='ssh jseidler@crowdwisely.org'
alias crws.ly='ssh jseidler@crowdwise.ly'

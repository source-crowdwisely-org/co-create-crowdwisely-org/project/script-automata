#!/bin/env bash

# Change to test ssh connection
gi() { curl -L -s https://www.gitignore.io/api/$@; }

# Idea from --> https://codeburst.io/learn-how-to-create-custom-bash-commands-in-less-than-3-minutes-6d4ceadd9590
# Doesnt work... why???
lazygit() {
	git add .
	git commit -a -m $2
	git push
}

# Push an existing folder to git
#	arg2 -> remote repository where it will be pushed
#	Example: git remote add origin ssh://git@source.crowdwisely.org:2223/co-create/make-projects/that-overcomes-recognise-and-convert-speech-to-text.git
push_existing_folder_to_git() {
	git init --initial-branch=main && 
	git remote add origin $1 && 
	git add . && 
	git commit && 
	git push -u origin main
}

# Clone a git repo and add the 'source' and 'mirror' remotes. Also removes 'origin' remote
clone_git_repo_and_add_remotes() {
    # Check if the required argument (repository URL) is provided
    if [ $# -eq 0 ]; then
        echo "Usage: clone_git_repo_and_add_remotes <repository_url>"
        return 1
    fi

    # Clone the repository
    repo_url="$1"
    git clone "$repo_url"
    repo_name=$(basename "$repo_url" .git)

    # Change to the cloned repository's directory
    cd "$repo_name" || return 1

    # Determine the main directory
    main_dir=$(echo "$repo_url" | grep -oE '/(create|in-create|co-create)/')

    # Add the source and private-mirror remotes
    source_url="$repo_url"
    private_mirror_url=$(echo "$source_url" | sed -e 's/ssh:\/\/git@source\.crowdwise\.ly:2222/ssh:\/\/git@source\.crowdwisely\.org:2222/')
    git remote add source "$source_url"
    git remote add private-mirror "$private_mirror_url"

    # Add the public-mirror remote if the repository is in the "co-create" directory
    if [[ "$main_dir" == "/co-create/" ]]; then
        public_mirror_url=$(echo "$source_url" | sed -e 's/ssh:\/\/git@source\.crowdwise\.ly:2222/git@gitlab\.com:source-crowdwisely-org/' -e 's/co-create-crowdwisely-org/co-create/')
        git remote add public-mirror "$public_mirror_url"
    fi

    # Remove the origin remote
    git remote remove origin
}

# Push to all available git remotes
function git_push_all_remotes() {
    # Set a timeout in seconds (e.g., 10 seconds)
    timeout_duration=10

    for remote in $(git remote); do
        echo "Pushing to $remote..."
        # Attempt to push with a timeout
        timeout "$timeout_duration" git push "$remote" --all

        # Check the exit status of the timeout command
        exit_status=$?
        if [ $exit_status -eq 124 ]; then
            echo "Timeout reached for $remote. Skipping..."
        elif [ $exit_status -ne 0 ]; then
            echo "An error occurred while pushing to $remote. Skipping..."
        fi
    done
}

function swap_git_remotes() {
	# Check if exactly two arguments are provided
	if [ "$#" -ne 2 ]; then
	    echo "Usage: $0 <old_remote1_name> <old_remote2_name>"
	    exit 1
	fi
	
	# Assign arguments to variables for clarity
	old_remote1=$1
	old_remote2=$2
	temp_remote_name="temp-remote"
	
	# Renaming old_remote1 to a temporary name
	git remote rename $old_remote1 $temp_remote_name
	
	# Renaming old_remote2 to old_remote1
	git remote rename $old_remote2 $old_remote1
	
	# Renaming the temporary remote to old_remote2
	git remote rename $temp_remote_name $old_remote2
	
	# Verifying the change
	if git remote | grep -q $old_remote1 && git remote | grep -q $old_remote2; then
	    echo "Remotes have been swapped."
	else
	    echo "Failed to swap remotes. Please check the remote names and try again."
	fi
}

function install_aws_cli() {
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
	unzip awscliv2.zip
	sudo ./aws/install
}

function update_aws_cli() {
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
	unzip awscliv2.zip
	sudo ./aws/install --update
}

## Renaming all files in current directory by prepending or appending text
# Examples usage:
#	prepend "" "write-"
#
prepend() {
	for f in *$2; do mv "$f" "$2$f"; done
}

## Append string to filename
# Example usage:
#       append ".txt" "String-to-Append"
append() {
	for f in *$2; do mv "$f" "${f%$1}$2$1"; done
}

remove_string_from_filenames() { rename 's/'$2'/''/g' *; }

## Replace all files in current directory
## 	arg2 -> New Filename -> Example: "Test"
##	arg3 -> Format -> Example: ".jpg", "", ".pdf"
replace_all_file_names_in_current_directory() {
	for file in *$3
	do
		((var++))
		echo $var
		mv "$file" "${file/*$3/$1$var$2}"
	done
}

# Prepend a column in CSV File in Linux by passing parameters
## param 1 column with name - param 2 filename
csv_prepend_column() { awk  '{print "$1,"$0}' $2;  }

# Checking file sizes

# Checking directory sizes (source: https://stackoverflow.com/questions/11720079/how-can-i-see-the-size-of-files-and-directories-in-linux)
check_size_of_specific_directory(){  du -sh $1;  }

check_size_of_all_files_and_directories_in_current_directory(){  du -bsh *;  }

# Sort unsorted data in file and write to file
# [ref]{https://www.ibm.com/developerworks/community/blogs/58e72888-6340-46ac-b488-d31aa4058e9c/entry/linux_sort_command_sort_lines_of_text_files9?lang=en}
sort_data_from_file_and_write_to_file(){  sort $1 > sorted-$2;  }

# Docker functions
#alias inspec="sudo docker run -it --rm -v $(pwd):/share chef/inspec $@;"
removecontainers() {
	docker stop $(docker ps -aq)
	docker rm $(docker ps -aq)
}

removeimages() {
	docker rmi $(docker images -q)
}

armageddon() {
	removecontainers
	docker network prune -f
	docker rmi -f $(docker images --filter dangling=true -qa)
	docker volume rm $(docker volume ls --filter dangling=true -q)
	docker rmi -f $(docker images -qa)
}

# Creates directory and changes into that newly created directory
mkcd() {
	mkdir -p $1
	cd $1
}

# Shortens the asciidoctor command
adoc() {
	asciidoctor $1
	firefox $(find . -type f -iname \*.html)
	find . -type f -iname \*.html -delete
}

# Remove all files in this directory except for this one
# Example usage: 
#  remove-all-files-in-this-directory-except-for "file.txt"
remove_all_files_in_this_directory_except_for() {
	find . ! -name $1 -type f -exec rm -f {} +
}

# Download and Install Debian package
# Usage:
# 	download-and-install-debian-package https://github.com/mi-g/vdhcoapp/releases/download/v1.6.3/net.downloadhelper.coapp-1.6.3-1_amd64.deb "companion.deb" 

download_and_install_debian_package() {
	wget $1 -O "$2" && sudo apt install ./$2  
}

# As a System Administrator and GNU/Linux Enthusiast
# I want to gracefully restart my running docker services,
# so that I can reboot my server without fearing repercussions due to crashed services

# Scenario 1: All running Docker services shutdown gracefully before server reboot
graceful_docker_shutdown() {
	docker-compose -f /home/jseidler/co-create/use-resources/technologies/adopt/traefik-proxy/docker-compose.yml down
	docker-compose -f /home/jseidler/co-create/use-resources/technologies/docker/gitlab/docker-compose.yml down
	docker-compose -f /home/jseidler/co-create/use-resources/technologies/docker/nextcloud/docker-compose.yml down
	docker-compose -f /home/jseidler/co-create/use-resources/technologies/docker/nextcloud/database/docker-compose.yml down
}

# Challenges: 
#	* Home Directory in Debian 11 has Inconsistent State Across Several Machines
#	* Synchronise Local and Remote Files and Directories
#	$1 - path of files to be synchronised
synchronise_files_in_local_machine_home_directory_with_remote_server() {
	rsync -azP /home/jseidler/.bash_aliases jseidler@crowdwisely.org:/home/jseidler/.bash_aliases
	rsync -azP /home/jseidler/.bash_profile jseidler@crowdwisely.org:/home/jseidler/.bash_profile
	rsync -azP /home/jseidler/.bashrc jseidler@crowdwisely.org:/home/jseidler/.bashrc
	rsync -azP /home/jseidler/.zsh_aliases jseidler@crowdwisely.org:/home/jseidler/.zsh_aliases
	rsync -azP /home/jseidler/.zshrc jseidler@crowdwisely.org:/home/jseidler/.zshrc
	rsync -azP /home/jseidler/.zshrc.pre-oh-my-zsh jseidler@crowdwisely.org:/home/jseidler/.zshrc.pre-oh-my-zsh
	rsync -azP /home/jseidler/script/ jseidler@crowdwisely.org:/home/jseidler/script/
}

# Challenge: 
#	* Home Directory in Debian 11 has Inconsistent State Across Several Machines
#	* Synchronise Local and Remote Files and Directories
compare_local_and_remote_files() {
	ssh jseidler@crowdwisely.org "cat $1" | diff --side-by-side - $2
}

# List all open ports
# [reference-sources]: https://www.geeksforgeeks.org/ways-to-find-out-list-of-all-open-ports-in-linux/
list_open_ports() {
	sudo lsof -i -P -n | grep LISTEN
}

# Kill process
# $1 - process id
kill_process_by_id() {
	sudo kill -9 $1
}

# Match metadata title of document to name of file
match_metadata_title_to_name_of_file() {
	exiftool -overwrite_original -Title="$1" $1
}

#	As a Coder and Automator
#	I want to call python code from bash scripts,
#	so that I can more easily automate everyday tasks with the power of both.
#
#	Scenario 1: Calling "bash-calls-python-hello-world" function prints out "Hello, Python World!"
bash_calls_python_hello_world() {
	python3 ~/script/python/print_hello_world.py
}

## Text conversion functions ##

convert_to_title_case_apa_style() {
    local word result

    # Convert to lowercase first to start with a clean slate
    local phrase=$(echo "$1" | tr '[:upper:]' '[:lower:]')

    local idx=0
    # Split the phrase into an array
    set -A ADDR ${(s/ /)phrase}  # This line is Zsh-specific syntax for splitting a string
    for word in "${ADDR[@]}"; do
        case "$word" in
            [aA] | [aN] | [tT][hH][eE] | [aA][nN][dD] | [bB][uU][tT] | [fF][oO][rR] | [nN][oO][rR] | [oO][rR] | [sS][oO] | [yY][eE][tT] | [bB][yY] | [aA][tT] | [fF][oO][rR] | [wW][iI][tT][hH])
                if [[ $idx -eq 0 || $idx -eq $((${#ADDR[@]} - 1)) ]]; then
                    result+="$(echo "$word" | sed -e "s/./\u&/") "
                else
                    result+="$word "
                fi
            ;;
            *)
                result+="$(echo "$word" | sed -e "s/./\u&/") "
            ;;
        esac
        idx=$((idx+1))
    done

    # Handle colon or dash by capitalizing the next word
    result=$(echo "$result" | sed -e 's/\([:-]\) \([a-z]\)/\1 \u\2/g')

    echo "$result" | sed 's/ $//'
}

extract_tags_from_filename_and_insert_those_tags_into_file() {
    # Extract tags from the filename
    filename="$1"
    tags=$(echo "$filename" | grep -oP '\[.*?\]' | tr -d '[]' | tr '_' '\n')

    # Prepare the new content for the file
    header="== Tags kbd:[label]
:experimental:
"

    new_content="$header"
    while read -r tag; do
        new_content="${new_content}* $tag
"
    done <<< "$tags"

    new_content="${new_content}
"

    # Write the new content to the file
    # Use printf to ensure the right number of newlines
    #printf "%b" "$new_content" | head -c -1 > "$filename"
    printf "%s" "$new_content" > "$filename"
}



# co-create.crowdwisely.org
#	challenge.crowdwisely.org
#	ideate.crowdwisely.org
#	project.crowdwisely.org
#	resource.crowdwisely.org
#	learn.crowdwisely.org
co_create_challenge() {
  gitlab_host="source.crowdwisely.org" && # replace with your host
  gitlab_port="2222" &&

  ## Co-Create the building blocks of a challenge
  declare home_source_directory="$HOME/source" &&
  echo -n "Please name this challenge: " &&
  read -r name_challenge &&
  declare name_challenge_apa_title_case=$(convert_to_title_case_apa_style "$name_challenge") && # convert name_challenge to APA Title Case
  echo -n "Please prioritise this challenge: " &&
  read -r prioritise_challenge &&
  echo -n "Please label this challenge: " &&
  read -r label_tags &&
  declare challenge_name_as_filename="$(echo $name_challenge_apa_title_case | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
  declare date_of_today="$(date +"%Y-%m-%d")" &&
  declare challenge_status_date="challenge-IN-PROGRESS-1$date_of_today-TO-ONGOING" &&
  declare -u challenge_priority="$(echo $prioritise_challenge)" &&
  declare tags_labeled_without_commas_and_spaces="$(echo $label_tags | tr -d "," | tr " " "_")" &&
  declare challenge_name_as_directory="Challenge-of-$(echo $name_challenge_apa_title_case | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
  declare co_create_challenge_path="/co-create/challenge/$challenge_name_as_directory" &&
  declare template_directory_name="/Template-of-Challenge-of-Solve-Any-Challenge-With-the-Wisdom-of-Crowds" &&
  declare full_challenge_path="$home_source_directory$co_create_challenge_path" &&
  cp -r $home_source_directory/co-create/resource/adopt/Templates/co-create.crowdwise.ly/challenge$template_directory_name $full_challenge_path &&
  pushd "$full_challenge_path" &&
  declare co_created_challenge_title_file=Challenge-of-$challenge_name_as_filename\[source_co-create_$challenge_status_date\_name_Titles\].adoc &&
  mv Challenge-of-Solve-Any-Challenge** $co_created_challenge_title_file &&
  sed -i "s/Solve Any Challenge With the Wisdom of Crowds/$name_challenge_apa_title_case/" $co_created_challenge_title_file &&
  sed -i "s/challenge-STATUS-YYYYY-MM-DD/$challenge_status_date/" $co_created_challenge_title_file &&
  declare co_created_challenge_visibility_file=Visibility\[source_co-create_$challenge_status_date\_set\].adoc &&
  mv Visibility** $co_created_challenge_visibility_file &&
  declare co_created_challenge_status_file=Status\[source_co-create_$challenge_status_date\_report\].adoc &&
  mv Status** $co_created_challenge_status_file &&
  sed -i "s/challenge-STATUS-YYYYY-MM-DD/$challenge_status_date/" $co_created_challenge_status_file &&
  declare co_created_challenge_priority_file=Priority\[source_co-create_$challenge_status_date\_$challenge_priority\_set\].adoc &&
  mv Priority** $co_created_challenge_priority_file &&
  sed -i "s/LOWEST | LOW | MEDIUM | HIGH | HIGHEST/$challenge_priority/" $co_created_challenge_priority_file &&
  declare co_created_challenge_challengers_file=Challengers\[source_co-create_$challenge_status_date\_profile_name_Co-Creators_Challengers\].adoc &&
  mv Challengers** $co_created_challenge_challengers_file &&
  declare co_created_challenge_trigger_file=Trigger\[source_co-create_$challenge_status_date\_experience_describe\].adoc &&
  mv Trigger** $co_created_challenge_trigger_file &&
  declare co_created_challenge_rootcauses_file=Root-Causes\[source_co-create_$challenge_status_date\_rootcause_analyse\].adoc &&
  mv Root-Causes** $co_created_challenge_rootcauses_file &&
  declare co_created_challenge_story_file=Story\[source_co-create_$challenge_status_date\_specify_BDD-Stories\].adoc &&
  mv Story** $co_created_challenge_story_file &&
  declare co_created_challenge_tags_file=Tags\[source_co-create_$challenge_status_date\_label_$tags_labeled_without_commas_and_spaces\].adoc &&
  mv Tags** $co_created_challenge_tags_file &&
  $(extract_tags_from_filename_and_insert_those_tags_into_file $co_created_challenge_tags_file) &&
  declare co_created_challenge_ideas_file=Ideas-on-How-to-Solve-This-Challenge\[source_co-create_$challenge_status_date\_ideate_rate_comment\].adoc &&
  mv Ideas** $co_created_challenge_ideas_file &&
  declare co_created_challenge_projects_file=Projects-That-Solve-This-Challenge\[source_co-create_$challenge_status_date\_project_solve\].adoc &&
  mv Projects** $co_created_challenge_projects_file &&
  declare co_created_challenge_skills_file=Skills-to-Learn-From-This-Challenge\[source_co-create_$challenge_status_date\_learn_Skills_Wisdom\].adoc &&
  mv Skills** $co_created_challenge_skills_file &&
  declare co_created_challenge_linked_challenges_file=Linked-Challenges\[source_co-create_$challenge_status_date\_link\].adoc &&
  mv Linked-Challenges** $co_created_challenge_linked_challenges_file &&
  declare co_created_challenge_related_challenges_file=Related-Challenges\[source_co-create_$challenge_status_date\_relate\].adoc &&
  mv Related-Challenges** $co_created_challenge_related_challenges_file &&
  
  ## Co-Create the 'index.adoc' file from the building blocks of a challenge ##
  $(truncate -s 0 index.adoc)
  	
  # Title Building Block
  $(head -n 1 $co_created_challenge_title_file >> index.adoc) &&
  echo ":experimental:" >> index.adoc && # AsciiDoc needs this experimental attribute to enable specific macros
  echo ":toc: left" >> index.adoc && # AsciiDoc table of contents
  $(tail -n +3 $co_created_challenge_title_file >> index.adoc) &&
  
  # Visibility Building Block
  echo "[visibility]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_visibility_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_visibility_file >> index.adoc) &&
  	
  # Status Building Block
  echo "[status]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_status_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_status_file >> index.adoc) &&
  
  # Priority Building Block
  echo "[priority]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_priority_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_priority_file >> index.adoc) &&
  
  # Challengers Building Block
  echo "[challengers]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_challengers_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_challengers_file >> index.adoc) &&
  
  # Trigger Building Block
  echo "[trigger]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_trigger_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_trigger_file >> index.adoc) &&
  
  # Root-Causes Building Block
  echo "[root-causes]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_rootcauses_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_rootcauses_file >> index.adoc) &&
  
  # Story Building Block
  echo "[story]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_story_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_story_file >> index.adoc) &&
  
  # Tags Building Block
  echo "[tags]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_tags_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_tags_file >> index.adoc) &&

  # Ideas Building Block
  echo "[ideas-on-how-to-solve-this-challenge]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_ideas_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_ideas_file >> index.adoc) &&
  
  # Projects Building Block
  echo "[projects-that-solve-this-challenge]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_projects_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_projects_file >> index.adoc) &&

  # Skills Building Block
  echo "[skills-to-learn-with-this-challenge]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_skills_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_skills_file >> index.adoc) &&

  # Linked Challenges Building Block
  echo "[linked-challenges]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_linked_challenges_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_linked_challenges_file >> index.adoc) &&
  	
  # Related Challenges Building Block
  echo "[related-challenges]" >> index.adoc &&
  $(head -n 1 $co_created_challenge_related_challenges_file >> index.adoc) &&
  $(tail -n +3 $co_created_challenge_related_challenges_file >> index.adoc) &&
  
  echo "Co-Creation of challenge has begun!" #&&
  #push-existing-folder-to-git-repository "ssh://git@${gitlab_host}:${gitlab_port}/${co_create_challenge_path}.git" &&
  #popd
}

# Ideate on how to solve a specific challenge
co_create_ideate_how_to_solve_challenge() {
	gitlab_host="source.crowdwisely.org" && # replace with your host
	gitlab_port="2222" &&
	declare home_source_directory="$HOME/source" &&
	co_create_challenge_path="$home_source_directory/co-create/challenge/$(basename "$1")" &&
	declare ideas_name_as_directory="Ideas-on-How-to-Solve-$(basename "$co_create_challenge_path")" &&
	declare co_create_ideate_path="/co-create/ideate/$ideas_name_as_directory" &&
	declare template_directory_name="/Template-of-Ideas-on-How-to-Solve-Challenge-of-Solve-Any-Challenge-With-the-Wisdom-of-Crowds" &&
	declare full_ideas_path="$home_source_directory$co_create_ideate_path" &&
	cp -r $home_source_directory/co-create/resource/Templates/co-create-crowdwisely-org/ideate$template_directory_name $full_ideas_path &&
	declare challenge_tag_file=$(basename $co_create_challenge_path/label-Tags**) &&
	pushd "$full_ideas_path" && 
	mv label-Tags** $challenge_tag_file &&
	# TODO append 'ideate' tag to list of tags from challenge
	echo "Co-creation of ideas has begun!" &&
	# Push to repo ssh://git@source.crowdwisely.org:2222/co-create/ideate/
	#push-existing-folder-to-git-repository "ssh://git@${gitlab_host}:${gitlab_port}/${co_create_ideate_path}.git" &&
	popd

	#git init &&
	#git checkout -b main &&
	#git add . &&
	#git commit -m "Initial commit" &&
	#git remote add origin "" &&
	#git push -u origin main &&
}

# co-create.project to solve challenges
co_create_project_to_solve() {
	gitlab_host="source.crowdwisely.org" # replace with your host
	gitlab_port="2222"
	declare default_project_name="To-Solve" &&
	declare project_name="" &&
	declare home_source_directory="$HOME/source" &&
	echo -n "What is the name of the project that solves one or several challenges? " &&
	read -r name_project &&
	if [ "$name_project" = "" ]; 
	then
		project_name=$default_project_name
	else
		project_name=$name_project
	fi
	co_create_challenge_path="$home_source_directory/co-create/challenge/$(basename "$1")" &&
	declare -l project_name_as_directory="project-$(echo $project_name | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
	declare co_create_project_path="/co-create/project/$project_name_as_directory-$(basename "$co_create_challenge_path")" &&
	# TODO Prepare 'project' template
	declare template_directory_name="/project-to-solve" &&
	declare full_project_path="$home_source_directory$co_create_project_path" &&
	cp -r $home_source_directory/co-create/resource/adopt/Templates/co-create.crowdwise.ly/project$template_directory_name $full_project_path &&
	declare project_tag_file=$(basename $co_create_project_path/label-Tags**) &&
	pushd "$full_project_path" &&
	mv Project** Project-$project_name\[co-create_project_name-Title\].adoc && 
	mv label-Tags** $project_tag_file &&
	echo "Co-creation of project '$project_name' has begun!" &&
	#git init --initial-branch=main && 
	#git remote add origin "ssh://git@${gitlab_host}:${gitlab_port}${co_create_make_projects_path}.git" && 
	#git add . && 
	#git commit && 
	#git push -u origin main &&
	popd
}

# Project to overcome challenges
co_create_project_based_on_ideas_to_overcome_challenge_of() {
	gitlab_host="source.crowdwisely.org" # replace with your host
	gitlab_port="2222"
	declare defaul_project_name="project-based-on-ideas-to-overcome-challenge-of" &&
	declare name_project=$default_project_name &&
	declare home_source_directory="$HOME/source" &&
	echo -n "What is the name of the project that overcomes one or several challenges? " &&
	read -r name_project &&
	co_create_ideate_path="$home_source_directory/co-create/ideate/$(basename "$1")" &&
	declare -l project_name_as_directory="$(echo $name_project | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
	declare co_create_project_path="/co-create/project/$project_name_as_directory" &&
	# TODO Prepare 'project' template
	declare template_directory_name="/that-overcomes-challenges" &&
	declare full_project_path="$home_source_directory$co_create_project_path" &&
	cp -r $home_source_directory/co-create/resource/templates/co-create-crowdwisely-org/project$template_directory_name $full_project_path &&
	declare idea_tag_file=$(basename $co_create_ideate_path/label-Tags**) &&
	pushd "$full_project_path" &&
	mv label-Tags** $idea_tag_file &&
	echo "Co-creation of project '$name_project' has begun!" &&
	#git init --initial-branch=main && 
	#git remote add origin "ssh://git@${gitlab_host}:${gitlab_port}${co_create_make_projects_path}.git" && 
	#git add . && 
	#git commit && 
	#git push -u origin main &&
	popd
}

co_create_resource_to_make_project_succeed() {
	echo "TODO";
}

co_create_learn_to_become_wiser() {
	echo "TODO";
}

# create.crowdwisely.org
#	challenge.crowdwisely.org
#	ideate.crowdwisely.org
#	project.crowdwisely.org
#	resource.crowdwisely.org
#	learn.crowdwisely.org
create_challenge() {
  gitlab_host="source.crowdwisely.org" && # replace with your host
  gitlab_port="2222" &&
  declare home_source_directory="$HOME/source" &&
  echo -n "Please name this challenge: " &&
  read -r name_challenge &&
  declare name_challenge_apa_title_case=$(convert_to_title_case_apa_style "$name_challenge") && # convert name_challenge to APA Title Case
  echo -n "Please prioritise this challenge: " &&
  read -r prioritise_challenge &&
  echo -n "Please label this challenge: " &&
  read -r label_tags &&
  declare challenge_name_as_filename="$(echo $name_challenge_apa_title_case | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
  declare date_of_today="$(date +"%Y-%m-%d")" &&
  declare challenge_status_date="challenge-IN-PROGRESS-1$date_of_today-TO-ONGOING" &&
  declare -u challenge_priority="$(echo $prioritise_challenge)" &&
  declare tags_labeled_without_commas_and_spaces="$(echo $label_tags | tr -d "," | tr " " "_")" &&
  declare challenge_name_as_directory="Challenge-of-$(echo $name_challenge_apa_title_case | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
  declare create_challenge_path="/create/challenge/$challenge_name_as_directory" &&
  declare template_directory_name="/Template-of-Challenge-of-Solve-Any-Challenge-With-the-Wisdom-of-Crowds" &&
  declare full_challenge_path="$home_source_directory$create_challenge_path" &&
  cp -r $home_source_directory/co-create/resource/adopt/Templates/co-create.crowdwise.ly/challenge$template_directory_name $full_challenge_path &&
  pushd "$full_challenge_path" &&
  declare created_challenge_title_file=Challenge-of-$challenge_name_as_filename\[source_create_$challenge_status_date\_name_Titles\].adoc &&
  mv Challenge-of-Solve-Any-Challenge** $created_challenge_title_file &&
  sed -i "s/Solve Any Challenge With the Wisdom of Crowds/$name_challenge_apa_title_case/" $created_challenge_title_file &&
  sed -i "s/co-create/create/" $created_challenge_title_file &&
  sed -i "s/challenge-STATUS-YYYYY-MM-DD/$challenge_status_date/" $created_challenge_title_file &&
  declare created_challenge_visibility_file=Visibility\[source_create_$challenge_status_date\_set\].adoc &&
  mv Visibility** $created_challenge_visibility_file &&
  sed -i "s/co-create/create/" $created_challenge_visibility_file &&
  declare created_challenge_status_file=Status\[source_create_$challenge_status_date\_report\].adoc &&
  mv Status** $created_challenge_status_file &&
  sed -i "s/challenge-STATUS-YYYYY-MM-DD/$challenge_status_date/" $created_challenge_status_file &&
  declare created_challenge_priority_file=Priority\[source_create_$challenge_status_date\_$challenge_priority\].adoc &&
  mv Priority** $created_challenge_priority_file &&
  sed -i "s/LOWEST | LOW | MEDIUM | HIGH | HIGHEST/$challenge_priority/" $created_challenge_priority_file &&
  declare created_challenge_challengers_file=Challengers\[source_create_$challenge_status_date\_profile_name_Creators_Challengers\].adoc &&
  mv Challengers** $created_challenge_challengers_file &&
  declare created_challenge_trigger_file=Trigger\[source_create_$challenge_status_date\_experience_describe\].adoc &&
  mv Trigger** $created_challenge_trigger_file &&
  declare created_challenge_rootcauses_file=Root-Causes\[source_create_$challenge_status_date\_rootcause_analyse\].adoc &&
  mv Root-Causes** $created_challenge_rootcauses_file &&
  declare created_challenge_story_file=Story\[source_create_$challenge_status_date\_specify_BDD-Stories\].adoc &&
  mv Story** $created_challenge_story_file &&
  declare created_challenge_tags_file=Tags\[source_create_$challenge_status_date\_label_$tags_labeled_without_commas_and_spaces\].adoc &&
  mv Tags** $created_challenge_tags_file &&
  $(extract_tags_from_filename_and_insert_those_tags_into_file Tags*) &&
  declare created_challenge_ideas_file=Ideas-on-How-to-Solve-This-Challenge\[source_create_$challenge_status_date\_ideate_rate_comment\].adoc &&
  mv Ideas** $created_challenge_ideas_file &&
  declare created_challenge_projects_file=Projects-That-Solve-This-Challenge\[source_create_$challenge_status_date\_project_solve\].adoc &&
  mv Projects** $created_challenge_projects_file &&
  declare created_challenge_skills_file=Skills-to-Learn-From-This-Challenge\[source_create_$challenge_status_date\_learn_Skills_Wisdom\].adoc &&
  mv Skills** $created_challenge_skills_file &&
  declare created_challenge_linked_challenges_file=Linked-Challenges\[source_create_$challenge_status_date\_link\].adoc &&
  mv Linked-Challenges** $created_challenge_linked_challenges_file &&
  declare created_challenge_related_challenges_file=Related-Challenges\[source_create_$challenge_status_date\_relate\].adoc &&
  mv Related-Challenges** $created_challenge_related_challenges_file &&
  
  ## Co-Create the 'index.adoc' file from the building blocks of a challenge ##
  $(truncate -s 0 index.adoc)
  
  # Title Building Block
  $(head -n 1 $created_challenge_title_file >> index.adoc) &&
  echo ":experimental:" >> index.adoc && # AsciiDoc needs this experimental attribute to enable specific macros
  echo ":toc: left" >> index.adoc && # AsciiDoc table of contents
  $(tail -n +3 $created_challenge_title_file >> index.adoc) &&
  
  # Visibility Building Block
  echo "[visibility]" >> index.adoc &&
  $(head -n 1 $created_challenge_visibility_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_visibility_file >> index.adoc) &&
  
  # Status Building Block
  echo "[status]" >> index.adoc &&
  $(head -n 1 $created_challenge_status_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_status_file >> index.adoc) &&

  # Priority Building Block
  echo "[priority]" >> index.adoc &&
  $(head -n 1 $created_challenge_priority_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_priority_file >> index.adoc) &&

  # Challengers Building Block
  echo "[challengers]" >> index.adoc &&
  $(head -n 1 $created_challenge_challengers_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_challengers_file >> index.adoc) &&

  # Trigger Building Block
  echo "[trigger]" >> index.adoc &&
  $(head -n 1 $created_challenge_trigger_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_trigger_file >> index.adoc) &&
  
  # Root-Causes Building Block
  echo "[root-causes]" >> index.adoc &&
  $(head -n 1 $created_challenge_rootcauses_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_rootcauses_file >> index.adoc) &&
  
  # Story Building Block
  echo "[story]" >> index.adoc &&
  $(head -n 1 $created_challenge_story_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_story_file >> index.adoc) &&
  
  # Tags Building Block
  echo "[tags]" >> index.adoc &&
  $(head -n 1 $created_challenge_tags_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_tags_file >> index.adoc) &&

  # Ideas Building Block
  echo "[ideas-on-how-to-solve-this-challenge]" >> index.adoc &&
  $(head -n 1 $created_challenge_ideas_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_ideas_file >> index.adoc) &&

  # Projects Building Block
  echo "[projects-that-solve-this-challenge]" >> index.adoc &&
  $(head -n 1 $created_challenge_projects_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_projects_file >> index.adoc) &&

  # Skills Building Block
  echo "[skills-to-learn-with-this-challenge]" >> index.adoc &&
  $(head -n 1 $created_challenge_skills_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_skills_file >> index.adoc) &&
  
  # Linked Challenges Building Block
  echo "[linked-challenges]" >> index.adoc &&
  $(head -n 1 $created_challenge_linked_challenges_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_linked_challenges_file >> index.adoc) &&
  	
  # Related Challenges Building Block
  echo "[related-challenges]" >> index.adoc &&
  $(head -n 1 $created_challenge_related_challenges_file >> index.adoc) &&
  $(tail -n +3 $created_challenge_related_challenges_file >> index.adoc) &&
  echo "Creation of challenge has begun!" #&&
  #push-existing-folder-to-git-repository "ssh://git@${gitlab_host}:${gitlab_port}/${create_challenges_path}.git" &&
  #popd
}

# Ideate how to overcome a specific challenge
create_ideate_how_to_overcome_challenge() {
	gitlab_host="source.crowdwisely.org" && # replace with your host
	gitlab_port="2222" &&
	declare home_source_directory="$HOME/source" &&
	create_challenge_path="$home_source_directory/create/challenges/$(basename "$1")" &&
	declare -l ideas_name_as_directory="ideas-on-how-to-overcome-$(basename "$create_challenges_path")" &&
	declare create_ideate_path="/create/ideate/$ideas_name_as_directory" &&
	declare template_directory_name="/ideas-on-how-to-overcome-challenge-of-overcome-any-challenge-with-the-wisdom-of-crowds-template" &&
	declare full_ideas_path="$home_source_directory$create_ideate_path" &&
	cp -r $home_source_directory/co-create/resource/templates/co-create-crowdwisely-org/ideate$template_directory_name $full_ideas_path &&
	declare challenge_tag_file=$(basename $create_challenge_path/label-Tags**) &&
	pushd "$full_ideas_path" && 
	mv label-Tags** $challenge_tag_file &&
	# TODO append 'ideate' tag to list of tags from challenge
	echo "Creation of ideas has begun!" &&
	# Push to repo ssh://git@source.crowdwisely.org:2222/create/conceive-ideas/
	#push-existing-folder-to-git-repository "ssh://git@${gitlab_host}:${gitlab_port}/${create_ideate_path}.git" &&
	popd

	#git init &&
	#git checkout -b main &&
	#git add . &&
	#git commit -m "Initial commit" &&
	#git remote add origin "" &&
	#git push -u origin main &&
}

# Project to solve one or several challenges
create_project_to_solve() {
	gitlab_host="source.crowdwisely.org" # replace with your host
	gitlab_port="2222"
	declare default_project_name="To-Solve" &&
	declare project_name="" &&
	declare home_source_directory="$HOME/source" &&
	echo -n "What is the name of the project that solves one or several challenges? " &&
	read -r name_project &&
	if [ "$name_project" = "" ]; 
	then
		project_name=$default_project_name
	else
		project_name=$name_project
	fi
	create_challenge_path="$home_source_directory/create/challenge/$(basename "$1")" &&
	declare -l project_name_as_directory="project-$(echo $project_name | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
	declare create_project_path="/create/project/$project_name_as_directory-$(basename "$create_challenge_path")" &&
	# TODO Prepare 'projects' template
	declare template_directory_name="/project-that-solves" &&
	declare full_project_path="$home_source_directory$create_project_path" &&
	cp -r $home_source_directory/co-create/resource/adopt/Templates/co-create.crowdwise.ly/project$template_directory_name $full_project_path &&
	declare project_tag_file=$(basename $create_project_path/label-Tags**) &&
	pushd "$full_project_path" &&
	mv Project** Project-$project_name\[create_project_name-Title\].adoc && 
	mv label-Tags** $project_tag_file &&
	echo "Creation of project '$project_name' has begun!" &&
	#git init --initial-branch=main && 
	#git remote add origin "ssh://git@${gitlab_host}:${gitlab_port}${create_make_projects_path}.git" && 
	#git add . && 
	#git commit && 
	#git push -u origin main &&
	popd
}

# in-create.crowdwisely.org
#	challenge.crowdwisely.org
#	ideate.crowdwisely.org
#	project.crowdwisely.org
#	resource.crowdwisely.org
#	learn.crowdwisely.org
in_create_challenge() {
  gitlab_host="source.crowdwisely.org" && # replace with your host
  gitlab_port="2222" &&
  
  ## In-Create the building blocks of a challenge
  declare home_source_directory="$HOME/source" &&
  echo -n "Please enter the name of the internal collective: " &&
  read -r internal_collective &&
  declare internal_collective_formatted="$(echo $internal_collective | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
  declare internal_collective_path="$home_source_directory/in-create/$internal_collective_formatted"
    # Check if internal collective directory exists
  if [ ! -d "$internal_collective_path" ]; then
    echo "Error: Internal collective '$internal_collective' not found under '$internal_collective_path'."
    return 1 # Exit the function with an error status
  fi
  echo -n "Please name this challenge: " &&
  read -r name_challenge &&
  declare name_challenge_apa_title_case=$(convert_to_title_case_apa_style "$name_challenge") && # convert name_challenge to APA Title Case
  echo -n "Please prioritise this challenge: " &&
  read -r prioritise_challenge &&
  echo -n "Please label this challenge: " &&
  read -r label_tags &&
  declare challenge_name_as_filename="$(echo $name_challenge_apa_title_case | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
  declare date_of_today="$(date +"%Y-%m-%d")" &&
  declare challenge_status_date="challenge-IN-PROGRESS-1$date_of_today-TO-ONGOING" &&
  declare -u challenge_priority="$(echo $prioritise_challenge)" &&
  declare tags_labeled_without_commas_and_spaces="$(echo $label_tags | tr -d "," | tr " " "_")" &&
  declare challenge_name_as_directory="Challenge-of-$(echo $name_challenge_apa_title_case | tr -d "\"" | tr " " "-" | tr "'" "-")" &&
  declare in_create_challenge_path="/in-create/$internal_collective_formatted/challenge/$challenge_name_as_directory" &&
  declare template_directory_name="/Template-of-Challenge-of-Solve-Any-Challenge-With-the-Wisdom-of-Crowds" &&
  declare full_challenge_path="$home_source_directory$in_create_challenge_path" &&
  cp -r $home_source_directory/co-create/resource/adopt/Templates/co-create.crowdwise.ly/challenge$template_directory_name $full_challenge_path &&
  pushd "$full_challenge_path" &&
  declare in_created_challenge_title_file=Challenge-of-$challenge_name_as_filename\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_name_Titles\].adoc &&
  mv Challenge-of-Solve-Any-Challenge** $in_created_challenge_title_file &&
  sed -i "s/Solve Any Challenge With the Wisdom of Crowds/$name_challenge_apa_title_case/" $in_created_challenge_title_file &&
  sed -i "s/challenge-STATUS-YYYYY-MM-DD/$challenge_status_date/" $in_created_challenge_title_file &&
  declare in_created_challenge_visibility_file=Visibility\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_set\].adoc &&
  mv Visibility** $in_created_challenge_visibility_file &&
  sed -i "s/co-create/in-create, $internal_collective_formatted/" $in_created_challenge_visibility_file &&
  declare in_created_challenge_status_file=Status\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_report\].adoc &&
  mv Status** $in_created_challenge_status_file &&
  sed -i "s/challenge-STATUS-YYYYY-MM-DD/$challenge_status_date/" $in_created_challenge_status_file &&
  declare in_created_challenge_priority_file=Priority\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_$challenge_priority\_set\].adoc &&
  mv Priority** $in_created_challenge_priority_file &&
  sed -i "s/LOWEST | LOW | MEDIUM | HIGH | HIGHEST/$challenge_priority/" $in_created_challenge_priority_file &&
  declare in_created_challenge_challengers_file=Challengers\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_profile_name_In-Creators_Challengers\].adoc &&
  mv Challengers** $in_created_challenge_challengers_file &&
  declare in_created_challenge_trigger_file=Trigger\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_experience_describe\].adoc &&
  mv Trigger** $in_created_challenge_trigger_file &&
  declare in_created_challenge_rootcauses_file=Root-Causes\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_rootcause_analyse\].adoc &&
  mv Root-Causes** $in_created_challenge_rootcauses_file &&
  declare in_created_challenge_story_file=Story\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_specify_BDD-Stories\].adoc &&
  mv Story** $in_created_challenge_story_file &&
  declare in_created_challenge_tags_file=Tags\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_label_$tags_labeled_without_commas_and_spaces\].adoc &&
  mv Tags** $in_created_challenge_tags_file &&
  $(extract_tags_from_filename_and_insert_those_tags_into_file $in_created_challenge_tags_file) &&
  declare in_created_challenge_ideas_file=Ideas-on-How-to-Solve-This-Challenge\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_ideate_rate_comment\].adoc &&
  mv Ideas** $in_created_challenge_ideas_file &&
  declare in_created_challenge_projects_file=Projects-That-Solve-This-Challenge\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_project_solve\].adoc &&
  mv Projects** $in_created_challenge_projects_file &&
  declare in_created_challenge_skills_file=Skills-to-Learn-From-This-Challenge\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_learn_Skills_Wisdom\].adoc &&
  mv Skills** $in_created_challenge_skills_file &&
  declare in_created_challenge_linked_challenges_file=Linked-Challenges\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_link\].adoc &&
  mv Linked-Challenges** $in_created_challenge_linked_challenges_file &&
  declare in_created_challenge_related_challenges_file=Related-Challenges\[source_in-create_$internal_collective_formatted\_$challenge_status_date\_relate\].adoc &&
  mv Related-Challenges** $in_created_challenge_related_challenges_file &&

  ## In-Create the 'index.adoc' file from the building blocks of a challenge ##
  $(truncate -s 0 index.adoc)

  # Title Building Block
  $(head -n 1 $in_created_challenge_title_file >> index.adoc) &&
  echo ":experimental:" >> index.adoc && # AsciiDoc needs this experimental attribute to enable specific macros
  echo ":toc: left" >> index.adoc && # AsciiDoc table of contents
  $(tail -n +3 $in_created_challenge_title_file >> index.adoc) &&

  # Visibility Building Block
  echo "[visibility]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_visibility_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_visibility_file >> index.adoc) &&

  # Status Building Block
  echo "[status]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_status_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_status_file >> index.adoc) &&

  # Priority Building Block
  echo "[priority]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_priority_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_priority_file >> index.adoc) &&

  # Challengers Building Block
  echo "[challengers]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_challengers_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_challengers_file >> index.adoc) &&

  # Trigger Building Block
  echo "[trigger]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_trigger_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_trigger_file >> index.adoc) &&

  # Root-Causes Building Block
  echo "[root-causes]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_rootcauses_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_rootcauses_file >> index.adoc) &&

  # Story Building Block
  echo "[story]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_story_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_story_file >> index.adoc) &&

  # Tags Building Block
  echo "[tags]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_tags_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_tags_file >> index.adoc) &&

  # Ideas Building Block
  echo "[ideas-on-how-to-solve-this-challenge]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_ideas_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_ideas_file >> index.adoc) &&

  # Projects Building Block
  echo "[projects-that-solve-this-challenge]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_projects_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_projects_file >> index.adoc) &&

  # Skills Building Block
  echo "[skills-to-learn-with-this-challenge]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_skills_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_skills_file >> index.adoc) &&

  # Linked Challenges Building Block
  echo "[linked-challenges]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_linked_challenges_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_linked_challenges_file >> index.adoc) &&
	
  # Related Challenges Building Block
  echo "[related-challenges]" >> index.adoc &&
  $(head -n 1 $in_created_challenge_related_challenges_file >> index.adoc) &&
  $(tail -n +3 $in_created_challenge_related_challenges_file >> index.adoc) &&

  echo "In-Creation of challenge has begun!" #&&
  #push-existing-folder-to-git-repository "ssh://git@${gitlab_host}:${gitlab_port}/${in_create_challenge_path}.git" &&
  #popd
}


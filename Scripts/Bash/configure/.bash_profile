#
# ~/.bash_profile
#

# Load .profile, containing login, non-bash related initializations.
[[ -f ~/.profile ]] && . ~/.profile

# Load .bashrc, containing non-login related bash initializations.
[[ -f ~/.bashrc ]] && . ~/.bashrc

# Load .bash_functions containing useful commands
[[ -f ~/source/co-create/make-projects/script-automata/scripts/bash/bash_functions ]] && . ~/'source'/co-create/make-projects/script-automata/scripts/bash/bash_functions

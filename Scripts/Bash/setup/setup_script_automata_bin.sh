#!/bin/bash

# Define the base directory for Script Automata and the scripts directory
BASE_DIR="$HOME/source/co-create/project/Script-Automata"
SCRIPTS_DIR="$BASE_DIR/Scripts/Bash"
BIN_DIR="$BASE_DIR/bin"

# Create the bin directory if it doesn't already exist
mkdir -p "$BIN_DIR"

# Function to create symbolic links for bash scripts
link_scripts() {
    local source_dir=$1
    local target_dir=$2

    # Find all bash scripts in the source directory and create symbolic links in the target directory
    find "$source_dir" -type f -name "*.sh" | while read script_path; do
        script_name=$(basename "$script_path" .sh) # Remove the .sh extension
        ln -sfn "$script_path" "$target_dir/$script_name"
        echo "Linked $script_path to $target_dir/$script_name"
    done
}

# Main execution starts here
# Loop through each verb directory under Scripts/Bash and link its scripts
for verb_dir in "$SCRIPTS_DIR"/*; do
    if [ -d "$verb_dir" ]; then
        link_scripts "$verb_dir" "$BIN_DIR"
    fi
done

# Instructions for adding the bin directory to PATH, to be done manually or through this script
echo "To add the Script Automata bin directory to your PATH, add the following line to your .bashrc or .profile:"
echo "export PATH=\"$BIN_DIR:\$PATH\""


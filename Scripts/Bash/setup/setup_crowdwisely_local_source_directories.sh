#!/bin/bash

mkdir ~/'source' &&

mkdir ~/'source'/create &&
mkdir ~/'source'/create/experience &&
mkdir ~/'source'/create/rootcause &&
mkdir ~/'source'/create/challenge &&
mkdir ~/'source'/create/ideate &&
mkdir ~/'source'/create/project &&
mkdir ~/'source'/create/resource &&
mkdir ~/'source'/create/resource/adopt &&
mkdir ~/'source'/create/resource/trial &&
mkdir ~/'source'/create/resource/assess &&
mkdir ~/'source'/create/resource/hold &&
mkdir ~/'source'/create/resource/deprecate &&
mkdir ~/'source'/create/learn &&
mkdir ~/'source'/create/learn/experiment &&

mkdir ~/'source'/co-create &&
mkdir ~/'source'/co-create/experience &&
mkdir ~/'source'/co-create/rootcause &&
mkdir ~/'source'/co-create/challenge &&
mkdir ~/'source'/co-create/ideate &&
mkdir ~/'source'/co-create/project &&
mkdir ~/'source'/co-create/resource &&
mkdir ~/'source'/co-create/resource/adopt &&
mkdir ~/'source'/co-create/resource/trial &&
mkdir ~/'source'/co-create/resource/assess &&
mkdir ~/'source'/co-create/resource/hold &&
mkdir ~/'source'/co-create/resource/deprecate &&
mkdir ~/'source'/co-create/learn &&
mkdir ~/'source'/co-create/learn/experiment &&

mkdir ~/'source'/in-create

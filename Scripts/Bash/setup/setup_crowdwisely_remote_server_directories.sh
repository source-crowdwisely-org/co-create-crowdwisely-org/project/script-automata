#!/bin/bash

mkdir ~/create &&
mkdir ~/create/experience &&
mkdir ~/create/rootcause &&
mkdir ~/create/challenge &&
mkdir ~/create/ideate &&
mkdir ~/create/project &&
mkdir ~/create/resource &&
mkdir ~/create/resource/adopt &&
mkdir ~/create/resource/trial &&
mkdir ~/create/resource/assess &&
mkdir ~/create/resource/hold &&
mkdir ~/create/resource/deprecate &&
mkdir ~/create/learn &&
mkdir ~/create/learn/experiment &&

mkdir ~/co-create &&
mkdir ~/co-create/experience &&
mkdir ~/co-create/rootcause &&
mkdir ~/co-create/challenge &&
mkdir ~/co-create/ideate &&
mkdir ~/co-create/project &&
mkdir ~/co-create/resource &&
mkdir ~/co-create/resource/adopt &&
mkdir ~/co-create/resource/trial &&
mkdir ~/co-create/resource/assess &&
mkdir ~/co-create/resource/hold &&
mkdir ~/co-create/resource/deprecate &&
mkdir ~/co-create/learn &&
mkdir ~/co-create/learn/experiment &&

mkdir ~/in-create

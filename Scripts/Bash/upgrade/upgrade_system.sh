#!/bin/env bash
# -------------------------------------------------------------------------
# Check which OS you are using and then perform the appropriate update process.
# -------------------------------------------------------------------------

upgrayedd() {
    # Determine the OS
    if [ -f /etc/os-release ]; then
        . /etc/os-release
    else
        echo "Error: Unable to determine OS. /etc/os-release not found."
        exit 1
    fi

    # Update and upgrade the system based on the OS
    case $ID in
        manjaro)
            echo "Updating Manjaro..."
            sudo pacman -Syu
            ;;
        ubuntu)
            echo "Updating Ubuntu..."
            sudo apt update && sudo apt upgrade -y && sudo snap refresh && sudo apt autoremove -y && sudo apt autoclean
            ;;
        endeavouros)
            echo "Updating EndeavourOS..."
            yay -Syu
            ;;
        debian)
            echo "Updating Debian..."
            sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y && sudo apt autoclean
            ;;
        *)
            echo "Error: Unsupported OS. Unable to perform update."
            exit 1
            ;;
    esac
}

# Execute the function
upgrayedd

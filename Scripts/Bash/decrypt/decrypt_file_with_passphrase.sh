#!/bin/env bash
# -------------------------------------------------------------------------
# Decrypt a GPG-encrypted file with a passphrase
# -------------------------------------------------------------------------

decrypt_file_with_passphrase() {
  # Check for exactly one argument
  if [ $# -ne 1 ]; then
    echo "Usage: $(basename $0) <input_file>"
    exit 1
  fi
  
  local input_file="$1"
  
  # Determine the output file name by removing the .gpg extension
  local output_file="${input_file%.gpg}"
  
  # Use gpg for symmetric decryption of the input file with a passphrase
  gpg --output "$output_file" --decrypt "$input_file"
  
  # Check if gpg command succeeded
  if [ $? -eq 0 ]; then
    echo "File decrypted with passphrase successfully: $output_file"
  else
    echo "Decryption failed."
    exit 2
  fi
}

# Call the function with the script's first argument
decrypt_file_with_passphrase "$1"

#!/bin/env bash
# -------------------------------------------------------------------------
# Encrypt a file using GPG with a passphrase
# -------------------------------------------------------------------------

encrypt_file_with_passphrase() {
  # Check if exactly one argument is provided
  if [ $# -ne 1 ]; then
    echo "Usage: $(basename $0) <input_file>"
    exit 1
  fi
  
  local input_file="$1"
  local output_file="${input_file}.gpg"
  
  # Use gpg to symmetrically encrypt the input file with a passphrase
  gpg --output "$output_file" --symmetric --cipher-algo AES256 "$input_file"
  
  # Check if gpg command succeeded
  if [ $? -eq 0 ]; then
    echo "File encrypted with passphrase successfully: $output_file"
  else
    echo "Encryption failed."
    exit 2
  fi
}

# Call the function with the script's first argument
encrypt_file_with_passphrase "$1"

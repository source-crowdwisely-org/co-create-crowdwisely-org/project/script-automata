#!/bin/bash

# Ensure we're in a Git repository
if ! git rev-parse --git-dir > /dev/null 2>&1; then
    echo "Error: must be run from within a Git repository"
    exit 1
fi

# Push main branch to GitLab remotes
git push 'source' main
git push public-mirror main
git push private-mirror main
git push github-mirror github-main:main


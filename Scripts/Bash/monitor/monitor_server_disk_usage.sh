#!/bin/bash

# Threshold for disk usage percentage
THRESHOLD=75

# Function to get current disk usage of /dev/vda1
get_disk_usage() {
    # Use df command to get disk usage and awk to parse the output for /dev/vda1
    echo $(df -h | awk '$1 == "/dev/vda1" {print $5}' | sed 's/%//')
}

# Get current disk usage
current_usage=$(get_disk_usage)

# Print current disk usage for debugging
echo "Current disk usage: $current_usage%"

# Check if current usage is greater than or equal to the threshold
if [ "$current_usage" -ge "$THRESHOLD" ]; then
    # Send notification
    echo "Warning: Disk usage for /dev/vda1 is over 75%. Current usage: $current_usage%. Please consider cleaning up or increasing disk capacity."
fi


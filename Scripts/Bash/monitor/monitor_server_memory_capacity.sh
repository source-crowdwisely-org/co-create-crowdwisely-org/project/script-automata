#!/bin/bash

# Threshold for server memory usage percentage
THRESHOLD=75

# Function to get current memory usage
get_memory_usage() {
    # Use free command to get memory usage and awk to parse the output
    echo $(free | awk '/Mem:/ {printf "%.0f", $3/$2 * 100}')
}

# Get current memory usage
current_usage=$(get_memory_usage)

# Print current memory usage for debugging
echo "Current memory usage: $current_usage%"

# Check if current usage is greater than or equal to the threshold
if [ "$current_usage" -ge "$THRESHOLD" ]; then
    # Send notification
    echo "Over 75% of your server memory capacity has been reached. Please backup your data immediately and increase memory capacity!"
fi


#!/bin/env bash

# Define the Conda environment name
ENV_NAME="Image-to-Text"

# Dynamically set the project root directory (adjust as necessary)
SCRIPT_AUTOMATA_SCRIPTS_PATH="$HOME/source/co-create/project/Script-Automata/Scripts"

# Activate Conda environment and install packages function
activate_conda_environment() {
    # Initialize Conda in this script session
    source "$(conda info --base)/etc/profile.d/conda.sh"
    
    # Activate the Conda environment
    echo "Activating Conda environment '$ENV_NAME'."
    conda activate $ENV_NAME

}

install_required_packages() {
    # Ensure that all required Python packages are installed
    echo "Installing required Python packages..."
    conda install -n $ENV_NAME pillow pytesseract -y
    # Install any other required packages here
}

# Check if the correct Conda environment exists and create it if it doesn't
if ! conda info --envs | grep -qw $ENV_NAME; then
    echo "Creating Conda environment '$ENV_NAME'."
    conda create --name $ENV_NAME python=3.8 -y
else
    echo "Conda environment '$ENV_NAME' already exists."
    activate_conda_environment
    install_required_packages
fi

# Set PYTHONPATH to include the project root directory
export PYTHONPATH="${PYTHONPATH}:${SCRIPT_AUTOMATA_SCRIPTS_PATH}"

# Run your Python script
# Assuming the usage of the script requires passing an image path, you'll need to do that when calling this bash script
echo "Running Python script..."
echo -e "=============================\n"

python -m Python.extract.extract_text_from_image "$1"

echo "============================="
echo "Action completed."


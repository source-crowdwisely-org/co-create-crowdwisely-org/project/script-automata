# A mapping from ISO 639-1 codes to Tesseract's expected ISO 639-2/T codes.
language_code_mapping = {
    'en': 'eng',
    'es': 'spa',
    'fr': 'fra',
    'de': 'deu',
    'zh': 'chi_sim',  # Assuming simplified Chinese
    'ru': 'rus',
    'ar': 'ara',
    # Add more mappings as needed.
}

def map_language_code(iso6391_code):
    """
    Maps an ISO 639-1 language code to an ISO 639-2/T code used by Tesseract.

    Parameters:
    - iso6391_code: The ISO 639-1 two-letter language code.

    Returns:
    - The corresponding ISO 639-2/T code if available, otherwise returns 'eng'.
    """
    return language_code_mapping.get(iso6391_code, 'eng')


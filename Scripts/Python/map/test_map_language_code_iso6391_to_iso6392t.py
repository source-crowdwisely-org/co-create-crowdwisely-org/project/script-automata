from map_language_code_iso6391_to_iso6392t import map_language_code

# Test cases with ISO 639-1 codes and their expected ISO 639-2/T mappings
test_cases = {
    'en': 'eng',  # English
    'es': 'spa',  # Spanish
    'fr': 'fra',  # French
    'de': 'deu',  # German
    'zh': 'chi_sim',  # Chinese simplified
    'ru': 'rus',  # Russian
    'ar': 'ara',  # Japanese
    'xx': 'eng',  # Test for an unsupported language code, expecting default 'eng'
}

def test_map_language_code():
    all_passed = True
    for iso6391, expected_iso6392t in test_cases.items():
        mapped_code = map_language_code(iso6391)
        if mapped_code == expected_iso6392t:
            print(f"PASS: {iso6391} correctly mapped to {mapped_code}")
        else:
            print(f"FAIL: {iso6391} was mapped to {mapped_code} instead of {expected_iso6392t}")
            all_passed = False
    return all_passed

if __name__ == "__main__":
    if test_map_language_code():
        print("All tests passed!")
    else:
        print("Some tests failed.")


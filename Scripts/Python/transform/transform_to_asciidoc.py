import sys
import re
import time
import requests
from bs4 import BeautifulSoup

def extract_document_header_and_body(content):
    """
    Extracts the document header and body from the provided content.
    
    Parameters:
    - content (str): The raw content of the document.

    Returns:
    - tuple: A tuple containing the header and the body as separate strings.
    """
    # Split the content using a delimiter (assuming '====' in this case)
    parts = content.split("====", 1) 
    
    # If there's a header and body, return them separately
    if len(parts) == 2:
        return parts[0].strip(), "'''" + parts[1].strip()
    # If there's no header, return an empty string for the header and the whole content as the body
    else:
        return "", content.strip()

def insert_document_header_back(header, processed_content):
    """
    Inserts the document header back to the processed content.

    Parameters:
    - header (str): The extracted document header from the original content.
    - processed_content (str): The processed content where the header needs to be inserted.

    Returns:
    - str: The final content with the header reinserted.
    """
    # If the header is not empty, concatenate it with the processed content
    if header:
        return header + "\n\n" + processed_content
    # If the header is empty, just return the processed content
    else:
        return processed_content

def convert_horizontal_lines(content):
    """Replaces horizontal lines represented by "====" with "'''."""
    return content.replace("====", "'''")

def extract_sections(content):
    """
    Extracts sections with "kbd:" from the content using regex.
    Stores the sections in a dictionary with keys derived from content inside the brackets after "kbd:".
    """
    sections = re.findall(r'(=+ kbd:.+?)(?=(=+ kbd:|$))', content, re.DOTALL)

    section_variables = {}
    for section in sections:
        match = re.search(r'kbd:\[(.+?)\]', section[0])
        if match:
            content_inside_brackets = match.group(1)
            split_content = content_inside_brackets.split(',')
            if len(split_content) > 1:
                section_key = split_content[1].strip()
            else:
                print(f"Unexpected format for section: {section[0]}")
                continue
        else:
            print("Error processing:", section[0])
            continue
        section_variables[section_key] = section[0]

    return section_variables

def dedent_section_content(section_content):
    """Dedents the provided content by removing a tab or four spaces from the start of each line."""
    # Split the section content by lines
    lines = section_content.split("\n")

    def is_line_indented(line):
        """Check if the line is indented with a tab or four spaces and return dedented line."""
        if line.startswith("\t"):
            return line[1:]
        elif line.startswith("    "):  # Four spaces
            return line[4:]
        else:
            return line

    # Dedent each line using the utility function
    dedented_lines = [is_line_indented(line) for line in lines]

    # Join the dedented lines to form the section content
    return "\n".join(dedented_lines)

def extract_url_title_tags(line):
    """Extract the URL, title, and tags from the provided line."""
    url_pattern = re.compile(r'\*\s(https?://\S+)\[(.*?)(?: kbd:\[(.*?)\])?\]')

    match = url_pattern.search(line)
    if match:
        url, title, tags = (g.strip() if g is not None else None for g in match.groups())
        return url, title, tags
    return None, None, None

def get_webpage_crawl_delay(robots_url):
    try:
        response = requests.get(robots_url, timeout=10)
        response.raise_for_status()

        # Check if there's a Crawl-delay defined
        crawl_delay_pattern = re.compile(r"Crawl-delay: (\d+)", re.IGNORECASE)
        match = crawl_delay_pattern.search(response.text)

        if match:
            return int(match.group(1))
        else:
            print(f"No crawl delay found in {robots_url}.")
            return None

    except requests.RequestException as e:
        print(f"Failed to retrieve content due to {str(e)}.")
        return None

def scrape_title_from_webpage(url):
    robots_url = "/".join(url.split("/")[:3]) + "/robots.txt"
    crawl_delay = get_webpage_crawl_delay(robots_url)

    if crawl_delay:
        print(f"Respecting a crawl delay of {crawl_delay} seconds.")
        time.sleep(crawl_delay)
    else:
        print("No crawl delay found. Proceeding without delay.")

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3"
    }

    try:
        response = requests.get(url, headers=headers, timeout=10)  # 10 seconds timeout
        response.raise_for_status()  # Raise an error if not a 200; e.g., 404, 500, etc.

        soup = BeautifulSoup(response.text, 'html.parser')
        meta_tag = soup.find('meta', {'name': 'dcterms.Title'})

        if meta_tag and 'content' in meta_tag.attrs:
            return meta_tag['content']

        # If no dcterms.Title is found, check for og:title
        meta_tag = soup.find('meta', {'property': 'og:title'})
        if meta_tag and 'content' in meta_tag.attrs:
            return meta_tag['content']

        # If no dcterms.Title or og:title is found, default to the standard <title> tag
        return soup.title.string.strip() if soup.title else None

    except requests.RequestException as e:
        print(f"Failed to retrieve content due to {str(e)}")
        return None  # If there's an issue with the request

def process_types(types_section_content):
    return dedent_section_content(types_section_content)

def process_the_rabbit_hole(the_rabbit_hole_section_content):
    the_rabbit_hole_content = dedent_section_content(the_rabbit_hole_section_content).strip() #Remove leading/trailing whitespaces

    def process_links(item):
        url, title, tags = extract_url_title_tags(item) 
        if url and title:
            # Ensure the formatting is consistent
            if tags:
                tags = f"kbd:[{', '.join(tag.strip() for tag in tags.split(','))}]"
            else:
                # If only the title is there, use default tags
                tags = f"kbd:[create, project, explore-TO-DO, The-Rabbit-Hole]"
    
            return f"* {url}[{title} {tags}]"

        else:
            # If the URL does not have a title and/or tags
            url_pattern = re.compile(r'\*\s(https?://\S+)')
            match = url_pattern.search(item)
            if not match:
                return item  # Return the original item if it doesn't match the pattern
    
            url = match.group(1)
            title = scrape_title_from_webpage(url)
            tags = f"kbd:[create, project, explore-TO-DO, The-Rabbit-Hole]"
    
            return f"* {url}[{title} {tags}]"

    lines = the_rabbit_hole_content.split("\n")[1:] # Skipping the section header
    processed_lines = [process_links(line) for line in lines]

    # Ensure there's no trailing whitespace and then add a newline at the end
    processed_content = "\n".join(processed_lines).strip()
    return f"== kbd:[explore, The-Rabbit-Hole]\n{processed_content}\n"

def process_quotes(quotes_section_content):
    def extract_quotes(quotes_section_content):
        """Extract quotes from the provided content."""
        # The updated pattern will match the quote header (with or without author name) and content
        # but exclude the existing separators
        quote_pattern = re.compile(r'(\[quote(?:,.*?)?\])\s*?____\s*?(.+?)\s*?(?=____\n\[quote|____\n==|\Z)', re.DOTALL)
        # Splitting each quote section
        return re.findall(quote_pattern, quotes_section_content)

    quotes = extract_quotes(quotes_section_content)

    # Process and reformat each quote
    formatted_quotes = []
    for quote_header, quote_content in quotes:
        formatted_quote = f"{quote_header}\n____\n{quote_content.strip()}\n"
        formatted_quotes.append(formatted_quote)

    return "== kbd:[list, Quotes]\n" + "\n".join(formatted_quotes)

def process_tags(tags_section_content):
    tags_content = tags_section_content.split("\n", 1)[1].strip()  # Get content after "label, Tags" line

    # Check if the tags are already in bullet-point format
    if tags_content.startswith('*'):
        tags = [tag.strip() for tag in tags_content.split("\n")]
    else:
        tags = [f"* {tag.strip()}" for tag in tags_content.split(",")]

    formatted_tags = "\n".join(tags)
    return f"== kbd:[label, Tags]\n{formatted_tags}\n"

def process_sources(sources_section_content):
    sources_content = dedent_section_content(sources_section_content).strip()

    def process_links(item):
        url, title, tags = extract_url_title_tags(item) 
        if url and title:
            title = title.strip()  # Ensure no unwanted spaces in title
            # Ensure the formatting is consistent
            if tags:
                tags = f"kbd:[{', '.join(tag.strip() for tag in tags.split(','))}]"
            else:
                # If only the title is there, use default tags
                tags = f"kbd:[create, project, reference, Sources]"

            return f"* {url}[{title} {tags}]"

        else:
            # If the URL does not have a title and/or tags
            url_pattern = re.compile(r'\*\s(https?://\S+)')
            match = url_pattern.search(item)
            if not match:
                return item  # Return the original item if it doesn't match the pattern
    
            url = match.group(1)
            title = scrape_title_from_webpage(url)
            tags = f"kbd:[create, project, reference, Sources]"
    
            return f"* {url}[{title} {tags}]"

    lines = sources_content.split("\n")[1:] # Skipping the section header
    processed_lines = [process_links(line) for line in lines]

    # Ensure there's no trailing whitespace and then add a newline at the end
    processed_content = "\n".join(processed_lines).strip()
    return f"== kbd:[reference, Sources]\n{processed_content}\n"

def reassemble_content(sections):
    assembled_content = "'''\n"
    for key, value in sections.items():
        assembled_content += value
        assembled_content += "\n"  # Always add a newline after each section
    assembled_content += "'''"
    return assembled_content

def process_media_objects(content):
    """Processes and formats media objects in the content."""
    content = convert_horizontal_lines(content)

    # Split the content by "===="
    raw_media_objects = content.split("'''")

    media_objects = []
    for raw_media in raw_media_objects:
        if raw_media.strip() == "":
            continue

        sections = extract_sections(raw_media)

        for section_key, section_content in sections.items():
            if section_key in section_processors:
                # Use the appropriate processing function for each section
                sections[section_key] = section_processors[section_key](section_content)

        formatted_media = reassemble_content(sections)
        media_objects.append(formatted_media)

    return media_objects

# Dictionary for section processing functions
# This allows adding new sections and their processing functions easily in the future
section_processors = {
    'Types': process_types,
    'The-Rabbit-Hole': process_the_rabbit_hole,
    'Tags': process_tags,
    'Quotes': process_quotes,
    'Sources': process_sources
}

if __name__ == "__main__":
    # Check if the required arguments are provided
    if len(sys.argv) < 3:
        print("Usage: python3 script_name.py input_file output_file")
        sys.exit(1)

    # Read the input file
    with open(sys.argv[1], 'r', encoding="utf-8") as f:
        content = f.read()

    # Extract document header and body
    header, body_content = extract_document_header_and_body(content)

    # Process the (pre-processed) content using your extract_and_format_media_objects function
    processed_media_objects = process_media_objects(body_content)

    # Join the processed media objects into a single string for output
    output_content = "\n".join(processed_media_objects)

    # Insert header back
    final_output_content = insert_document_header_back(header, output_content)

    # Write the final output content to the specified output file
    with open(sys.argv[2], 'w', encoding="utf-8") as f:
        f.write(final_output_content)

    print(f"Processed content written to {sys.argv[2]}")


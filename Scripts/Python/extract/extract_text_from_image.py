import sys
from PIL import Image
import pytesseract
from Python.detect.detect_language import detect_language  # Adjust import path as needed.
from Python.map.map_language_code_iso6391_to_iso6392t import map_language_code

def extract_text_from_image(image_path):
    """
    Extracts text from an image file using PyTesseract, with automatic language detection.

    Parameters:
    - image_path: Path to the image file from which to extract text.

    Returns:
    - Extracted text as a string.
    """
    try:
        image = Image.open(image_path)
        # Initial OCR pass to get text for language detection
        extracted_text = pytesseract.image_to_string(image, lang='eng')
        detected_lang = detect_language(extracted_text)
        mapped_lang_code = map_language_code(detected_lang)
        # Optional: redo OCR with the mapped language code
        extracted_text = pytesseract.image_to_string(image, lang=mapped_lang_code)
        return extracted_text
    except Exception as e:
        return f"An error occurred: {str(e)}"

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python extract_text_from_image.py <Image-File>")
    else:
        image_path = sys.argv[1]
        text = extract_text_from_image(image_path)
        print(text)


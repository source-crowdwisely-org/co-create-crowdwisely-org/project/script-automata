#!/usr/bin/env python

# Python program to read data from a text file line by line
# reference Sources
#   https://www.computerhope.com/issues/ch001721.htm#read-text

import sys

# opening the text file
with open(sys.argv[1],'r') as file:

    # reading each line
    for line in file:

        # display the lines
        print(line)

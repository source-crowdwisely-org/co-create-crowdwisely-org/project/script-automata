#!/usr/bin/env python

# Python program to read
# file word by word
# reference Sources
#   https://www.geeksforgeeks.org/file-handling-python/

import sys

# opening the text file
with open(sys.argv[1],'r') as file:

    # reading each line
    for line in file:

        # reading each word
        for word in line.split():

            # displaying the words
            print(word)

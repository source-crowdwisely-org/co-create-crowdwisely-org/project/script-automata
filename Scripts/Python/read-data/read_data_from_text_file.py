#!/usr/bin/env python

# Python program to read and print contents from file
# reference Sources
#   https://www.tutorialspoint.com/How-to-read-a-file-from-command-line-using-Python

import sys

# opening the text file
with open(sys.argv[1],'r') as file:

	# read the contents from file
	contents = file.read()
	
	# display the contents of the file
	print(contents)

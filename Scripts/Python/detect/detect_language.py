from langdetect import detect, LangDetectException

def detect_language(text):
    """
    Detects the language of the given text.

    Parameters:
    - text: Text to detect the language of.

    Returns:
    - Detected language code (ISO 639-1), defaults to 'eng' if detection fails.
    """
    try:
        return detect(text)
    except LangDetectException:
        return "eng"  # Default to English if detection fails


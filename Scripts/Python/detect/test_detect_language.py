from detect_language import detect_language

# List of test sentences in different languages
test_sentences = [
    ("This is an English sentence.", "English"),
    ("Este es un texto en español.", "Spanish"),
    ("Ceci est une phrase française.", "French"),
    ("Dies ist ein deutscher Satz.", "German"),
    ("这是一个中文句子。", "Chinese"),
    ("これは日本語の文です。", "Japanese"),
    ("Это русское предложение.", "Russian")
]

def test_language_detection():
    for sentence, expected_language in test_sentences:
        detected_lang = detect_language(sentence)
        print(f"Test sentence: '{sentence}'\nExpected language: {expected_language}\nDetected language code: {detected_lang}\n")

if __name__ == "__main__":
    test_language_detection()


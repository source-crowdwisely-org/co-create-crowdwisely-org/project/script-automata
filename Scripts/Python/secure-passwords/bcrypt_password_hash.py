import sys
import bcrypt
 
cost = 14

password = sys.argv[1]
passwordHash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt(cost))
passwordHash = passwordHash.decode('utf-8')
 
print(passwordHash)
